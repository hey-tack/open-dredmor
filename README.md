# Open Dredmor

A reimplementation of Dungeons of Dredmor.

# Discord

[Come chat about Open Dredmor](https://discord.gg/zTq9CG)

## What else do I need?

A full installation of Dungeons of Dredmor is required. Open Dredmor is an engine alone and doesn't contain copyrighted materials such as the game's assets.

## I Want to Know More

Check out the [WIKI](https://gitlab.com/open-dredmor/open-dredmor/-/wikis/home).